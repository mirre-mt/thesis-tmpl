{
  # check out https://git.dotya.ml/mirre-mt/thesis-tmpl/
  description = "Thesis tmpl";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
  };

  outputs = {
    self,
    nixpkgs,
    ...
  }: let
    # to work with older version of flakes
    lastModifiedDate =
      self.lastModifiedDate or self.lastModified or "19700101";
    supportedSystems = ["aarch64-linux" "x86_64-linux" "x86_64-darwin"];
    forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

    # ms, bc
    thesis = "";

    checkThesisType = {type}:
      if builtins.stringLength type == 0
      then "unknown"
      else type;
    thesisType = checkThesisType {type = thesis;};

    documentName = "thesis";
    pname = thesisType + "_" + documentName;
    version = "0.0.1";

    pkgs = forAllSystems (system: nixpkgs.legacyPackages.${system});
    nixpkgsFor = forAllSystems (
      system:
        import nixpkgs {
          inherit system;
          overlays = [self.overlays.default];
        }
    );
  in rec {
    formatter = forAllSystems (
      system:
        nixpkgsFor.${system}.alejandra
    );

    overlays.default = with pkgs;
      final: prev: {
        watcher = with final;
          writeScriptBin "watch" ''
            out=".latexmkout"
            mkdir "$out"
            latexmk \
              -pvc \
              -outdir="$out" \
              -pdf \
              -pdflatex="pdflatex -synctex=1 -file-line-error -interaction=nonstopmode" \
              -use-make ${documentName}.tex
            rm -r "$out"
          '';
      };

    packages = with pkgs;
      forAllSystems (system: {
        inherit (nixpkgsFor.${system}) watcher;

        default = nixpkgsFor.${system}.watcher;
      });

    apps = forAllSystems (system: rec {
      watch = {
        type = "app";
        program = "${self.packages."${system}".watcher}/bin/watch";
      };
      default = watch;
    });

    devShells = with pkgs;
      forAllSystems (system: let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [self.overlays.default];
        };

        watcher = pkgs.writeShellScriptBin "watcher" ''
          out=".latexmkout"
          mkdir "$out"
          latexmk \
            -pvc \
            -outdir="$out" \
            -pdf \
            -pdflatex="pdflatex -synctex=1 -file-line-error -interaction=nonstopmode" \
            -use-make ${documentName}.tex
          rm -r "$out"
        '';
      in {
        default = nixpkgsFor.${system}.mkShell {
          name = "${pname}-" + version;
          shellHook = ''
            echo " -- in ${pname} dev shell..."
          '';

          packages = with nixpkgsFor.${system}; [
            watcher

            gnumake
            alejandra
            distrobox
            podman
          ];
        };
      });
  };
}
