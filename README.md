# [`thesis-tmpl`](https://git.dotya.ml/mirre-mt/thesis-tmpl/)

this repo contains the official [Tomas Bata University](https://www.utb.cz/)
latex thesis template along with some opinionated quality-of-life goodies, such
as a Makefile and instructions for preparing a complete (Fedora based)
environment capable of compiling the latex files into a pdf.

## how to make this run
prerequisities:
* `git`
* `make`
* [`distrobox`](https://github.com/89luca89/distrobox)

alternatively, when using [Nix](https://builtwithnix.org/), just run `direnv
allow`, `nix develop` (with [flakes](https://nixos.wiki/wiki/Flakes) enabled)
or `nix shell` (when using nix without flakes).

### instructions:
* clone this repo and cd into it
```sh
git clone <repo url> && cd ./thesis-tmpl
```
* create a Fedora toolbox
```sh
# root not needed
distrobox-create -n f-toolbox -i registry.fedoraproject.org/fedora-toolbox:37
```
* enter the distrobox
```sh
distrobox enter f-toolbox
```
* install pkgs
```sh
# in the toolbox you just created
sudo dnf install -y $(<extra-packages)
```
* profit!
```sh
# in the toolbox
make watch
```

## LICENSE
Makefile, config files and these instructions are available under CC0, it is
unclear under what license is the latex template available.
